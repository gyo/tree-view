# tree-view

[https://tree-view.gyo.now.sh/](https://tree-view.gyo.now.sh/)

## Development

```
npm install
npm run dev
```

## Sample

```jsx
import { useTree } from "components/Tree";

const tree = {
  id: "root",
  nodes: [
    { id: "1" },
    {
      id: "2",
      nodes: [
        {
          id: "2-1",
        },
      ],
    },
  ],
};

export const Sample: React.FC = () => {
  const treeProps = useTree(tree.nodes);

  return <Tree {...treeProps} />;
};
```

## useTree Arguments

```
useTree(nodes[, options]);
```

| Argument                        | Type                | Required | Default | Description                                                            |
| ------------------------------- | ------------------- | -------- | ------- | ---------------------------------------------------------------------- |
| defaultNodes                    | `Node<T>[]`         | required |         | `type Node<T> = { id: string; parentId?: string; body?: T };` 型の配列 |
| options                         | `object`            | optional |         |                                                                        |
| options.defaultExpandedIds      | `string[]`          | optional |         | 初期描画時に `expanded` とする ID の配列                               |
| options.defaultSelectedIds      | `string[]`          | optional |         | 初期描画時に `selected` とする ID の配列                               |
| options.selectableIds           | `string[]`          | optional |         | 選択可能な ID の配列                                                   |
| options.onlyLeafNodesSelectable | `boolean`           | optional | `false` | 末端のノードのみ選択可能とするかどうか                                 |
| options.multiSelect             | `boolean`           | optional | `true`  | 複数選択可能とするかどうか                                             |
| options.onExpand                | `(params) => void;` | optional |         | 開閉時に実行する関数（詳細は後述）                                     |

### onExpand

```typescript
(
  currentNode: TreeNode<T>,
  direction: "open" | "close",
  append: (appendingNodes: Node<T>[]) => void
) => void;
```

| Argument    | Type                                  | Description                                    |
| ----------- | ------------------------------------- | ---------------------------------------------- |
| currentNode | `TreeNode<T>`                         | 開閉されたノード                               |
| direction   | `"open" | "close"`                    | 操作によってノードが開かれるのか閉じられるのか |
| append      | `(appendingNodes: Node<T>[]) => void` | `appendingNodes` をツリーに追加する関数        |

## Tree Component Props

```jsx
const treeProps = useTree(nodes[, options]);

<Tree {...treeProps} treeId renderNodeContent nodeStyle />
```

| Props             | Type                               | Required | Description                                                                              |
| ----------------- | ---------------------------------- | -------- | ---------------------------------------------------------------------------------------- |
| treeId            | `string`                           | optional | Tree 内部にある `input` 要素の `name` 属性の値（デフォルトでは Tree ごとにランダムな値） |
| inputType         | `"checkbox" | "radio"`             | optional | Tree 内部にある `input` 要素の `type` 属性の値（デフォルトでは `"checkbox"`）            |
| renderNodeContent | `(params) => JSX.Element \| null;` | optional | 各ノードのコンテンツをレンダリングする関数（詳細は後述）                                 |

### renderNodeContent

```typescript
(params) => JSX.Element | null;
```

| Argument                | Type                          | Description                                                                                                     |
| ----------------------- | ----------------------------- | --------------------------------------------------------------------------------------------------------------- |
| params.treeId           | `string`                      | Tree Component に渡された `treeId`                                                                              |
| params.treeNode         | `TreeNode<T>`                 |                                                                                                                 |
| params.selectable       | `boolean`                     | このノードが `selectable` かどうか                                                                              |
| params.expanded         | `boolean`                     | このノードが `expanded` かどうか                                                                                |
| params.selected         | `boolean`                     | このノードが `selected` かどうか                                                                                |
| params.visuallySelected | `boolean`                     | `selectable` ではないノードのうち、いずれかの先祖が `selected` なもの、もしくはすべての子孫が `selected` なもの |
| params.indeterminate    | `boolean`                     | `indeterminate` かどうか（子孫に `selected` な要素を持つ）                                                      |
| params.expand           | `() => void`                  | 開閉処理を実行する関数                                                                                          |
| params.select           | `() => void`                  | 選択処理を実行する関数                                                                                          |
| params.childTree        | `React.ReactNode | undefined` | 子孫ノードの `ReactNode`                                                                                        |
