import { Node, TreeNode } from "components/Tree";
import {
  buildTree,
  collectSelectableAncestorNodeIds,
  findNode,
  getAncestorsObject,
  getDescendantsObject,
  getParentObject,
  getSelectableIds,
  getSiblingsObject,
  normalizeExpandedIds,
  normalizeIndeterminateIds,
  normalizeSelectedIds,
  normalizeVisuallySelectedIds,
  remove,
} from "components/Tree/helpers";
import React from "react";

export type OnExpand<T> = (
  currentNode: TreeNode<T>,
  direction: "open" | "close",
  append: (appendingNodes: Node<T>[]) => void
) => void;

export const useTree = <T>(
  defaultNodes: Node<T>[],
  options?: {
    defaultExpandedIds?: Node<T>["id"][];
    defaultSelectedIds?: Node<T>["id"][];
    selectableIds?: Node<T>["id"][];
    onlyLeafNodesSelectable?: boolean;
    multiSelect?: boolean;
    onExpand?: OnExpand<T>;
  }
) => {
  const [nodes, setNodes] = React.useState(defaultNodes);

  // treeNodes
  const treeNodes = React.useMemo(() => {
    return buildTree(nodes);
  }, [nodes]);

  // treeNodes 同士の関係を表す値
  const siblingsObject = React.useMemo(() => {
    return getSiblingsObject({
      id: "root",
      nodes: treeNodes,
    });
  }, [treeNodes]);
  const parentObject = React.useMemo(() => {
    return getParentObject(treeNodes);
  }, [treeNodes]);
  const descendantsObject = React.useMemo(() => {
    return getDescendantsObject(treeNodes);
  }, [treeNodes]);
  const ancestorsObject = React.useMemo(() => {
    return getAncestorsObject(treeNodes);
  }, [treeNodes]);

  // 選択可能な treeNodes の一覧
  const selectableIds = React.useMemo(() => {
    return getSelectableIds(
      treeNodes,
      options?.selectableIds,
      options?.onlyLeafNodesSelectable
    );
  }, [options?.onlyLeafNodesSelectable, options?.selectableIds, treeNodes]);

  // 複数の treeNodes を選択可能かどうか
  const multiSelect = React.useMemo(() => {
    return options?.multiSelect ?? true;
  }, [options?.multiSelect]);

  // TreeView の UI State
  const [selectedIds, setSelectedIds] = React.useState(
    normalizeSelectedIds(
      options?.defaultSelectedIds ?? [],
      siblingsObject,
      parentObject,
      descendantsObject,
      multiSelect,
      selectableIds
    )
  );
  const [visuallySelectedIds, setVisuallySelectedIds] = React.useState(
    normalizeVisuallySelectedIds(
      options?.defaultSelectedIds ?? [],
      siblingsObject,
      parentObject,
      descendantsObject,
      multiSelect,
      selectableIds
    )
  );
  const [expandedIds, setExpandedIds] = React.useState(
    normalizeExpandedIds(
      options?.defaultExpandedIds ?? [],
      selectedIds,
      ancestorsObject
    )
  );
  const indeterminateIds = React.useMemo(() => {
    return normalizeIndeterminateIds(
      selectedIds,
      visuallySelectedIds,
      ancestorsObject
    );
  }, [ancestorsObject, selectedIds, visuallySelectedIds]);

  // TreeView の UI で利用する関数
  const append = React.useCallback(
    (appendingNodes: Node<T>[]) => {
      const nextNodes = [...nodes, ...appendingNodes];
      const nextTreeNodes = buildTree(nextNodes);
      const nextSiblingsObject = getSiblingsObject({
        id: "root",
        nodes: nextTreeNodes,
      });
      const nextParentObject = getParentObject(nextTreeNodes);
      const nextDescendantsObject = getDescendantsObject(nextTreeNodes);
      const nextSelectableIds = getSelectableIds(
        nextTreeNodes,
        options?.selectableIds,
        options?.onlyLeafNodesSelectable
      );

      setNodes(nextNodes);
      setSelectedIds(
        normalizeSelectedIds(
          selectedIds,
          nextSiblingsObject,
          nextParentObject,
          nextDescendantsObject,
          multiSelect,
          nextSelectableIds
        )
      );
      setVisuallySelectedIds(
        normalizeVisuallySelectedIds(
          selectedIds,
          nextSiblingsObject,
          nextParentObject,
          nextDescendantsObject,
          multiSelect,
          nextSelectableIds
        )
      );
    },
    [
      multiSelect,
      nodes,
      options?.onlyLeafNodesSelectable,
      options?.selectableIds,
      selectedIds,
    ]
  );

  const expand = React.useCallback(
    (operatedNodeId: Node<T>["id"]) => {
      let direction: "open" | "close" = "open";
      if (expandedIds.includes(operatedNodeId)) {
        direction = "close";
        setExpandedIds(remove(expandedIds, operatedNodeId));
      } else {
        setExpandedIds([...expandedIds, operatedNodeId]);
      }

      const operatedNode = findNode(treeNodes, operatedNodeId);
      if (operatedNode != null) {
        options?.onExpand?.(operatedNode, direction, append);
      }
    },
    [append, expandedIds, options, treeNodes]
  );

  const selectSingle = React.useCallback(
    (operatedNodeId: Node<T>["id"]) => {
      if (selectedIds.includes(operatedNodeId)) {
        // true -> false の場合
        setSelectedIds([]);
      } else {
        // false -> true の場合

        // operatedNodeId が selectableIds に含まれない場合は return する
        if (!(selectableIds[operatedNodeId] ?? false)) {
          return;
        }

        setSelectedIds([operatedNodeId]);
      }
    },
    [selectableIds, selectedIds]
  );

  const selectMultiple = React.useCallback(
    (operatedNodeId: Node<T>["id"]) => {
      if (selectedIds.includes(operatedNodeId)) {
        // true -> false の場合

        const removingSelectedNodeIds: Node<T>["id"][] = [];
        const removingVisuallySelectedNodeIds: Node<T>["id"][] = [];

        // 自身を false にする
        removingSelectedNodeIds.push(operatedNodeId);

        // ancestors を false にする
        const ancestorsNodeId = ancestorsObject[operatedNodeId] ?? [];
        removingSelectedNodeIds.push(...ancestorsNodeId);
        removingVisuallySelectedNodeIds.push(...ancestorsNodeId);

        // descendants を false にする
        const descendantsNodeId = descendantsObject[operatedNodeId] ?? [];
        removingSelectedNodeIds.push(...descendantsNodeId);
        removingVisuallySelectedNodeIds.push(...descendantsNodeId);

        const nextSelectedIds = remove(selectedIds, removingSelectedNodeIds);
        const nextVisuallySelectedIds = remove(
          visuallySelectedIds,
          removingVisuallySelectedNodeIds
        );
        setSelectedIds(nextSelectedIds);
        setVisuallySelectedIds(nextVisuallySelectedIds);
      } else {
        // false -> true の場合

        // operatedNodeId が selectableIds に含まれない場合は return する
        if (!(selectableIds[operatedNodeId] ?? false)) {
          return;
        }

        let addingSelectedNodeIds: Node<T>["id"][] = [];
        let addingVisuallySelectedNodeIds: Node<T>["id"][] = [];

        // 自身を true にする
        addingSelectedNodeIds.push(operatedNodeId);

        // descendants を true にする
        const descendantsNodeId = descendantsObject[operatedNodeId] ?? [];
        addingSelectedNodeIds.push(...descendantsNodeId);
        addingVisuallySelectedNodeIds.push(...descendantsNodeId);

        // ancestors を辿り、siblings が全て true の場合 parent を true にする
        addingSelectedNodeIds.push(
          ...collectSelectableAncestorNodeIds(
            operatedNodeId,
            [...selectedIds, ...visuallySelectedIds],
            siblingsObject,
            parentObject
          )
        );
        addingVisuallySelectedNodeIds.push(
          ...collectSelectableAncestorNodeIds(
            operatedNodeId,
            [...selectedIds, ...visuallySelectedIds],
            siblingsObject,
            parentObject
          )
        );

        // selectable なもので絞り込む
        addingSelectedNodeIds = addingSelectedNodeIds.filter((addingNodeId) => {
          return selectableIds[addingNodeId] ?? false;
        });

        // selectable でないもので絞り込む
        addingVisuallySelectedNodeIds = addingVisuallySelectedNodeIds.filter(
          (addingNodeId) => {
            return !(selectableIds[addingNodeId] ?? false);
          }
        );

        const nextSelectedIds = Array.from(
          new Set([...selectedIds, ...addingSelectedNodeIds])
        );
        const nextVisuallySelectedIds = Array.from(
          new Set([...visuallySelectedIds, ...addingVisuallySelectedNodeIds])
        );
        setSelectedIds(nextSelectedIds);
        setVisuallySelectedIds(nextVisuallySelectedIds);
      }
    },
    [
      ancestorsObject,
      descendantsObject,
      selectedIds,
      visuallySelectedIds,
      parentObject,
      selectableIds,
      siblingsObject,
    ]
  );

  const select = React.useCallback(
    (operatedNodeId: Node<T>["id"]) => {
      if (multiSelect) {
        selectMultiple(operatedNodeId);
      } else {
        selectSingle(operatedNodeId);
      }
    },
    [multiSelect, selectMultiple, selectSingle]
  );

  return {
    selectableIds: selectableIds,
    multiSelect,
    treeNodes,
    expandedIds,
    selectedIds,
    visuallySelectedIds,
    indeterminateIds,
    expand,
    select,
    append,
  };
};
