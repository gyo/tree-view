import { TreeNode } from "components/Tree";
import React from "react";

export type TreeNodeContentProps<T = undefined> = {
  // Tree props
  treeId: string;
  inputType?: "checkbox" | "radio";
  selectableIds: Record<TreeNode<T>["id"], boolean | undefined>;
  multiSelect: boolean;

  // Node props
  treeNode: TreeNode<T>;
  expandedIds: TreeNode<T>["id"][];
  selectedIds: TreeNode<T>["id"][];
  visuallySelectedIds: TreeNode<T>["id"][];
  indeterminateIds: TreeNode<T>["id"][];
  expand: (nodeId: TreeNode<T>["id"]) => void;
  select: (nodeId: TreeNode<T>["id"]) => void;
  renderNodeContent?: (params: {
    // Tree props
    treeId: string;

    // Node props
    treeNode: TreeNode<T>;
    selectable: boolean;
    expanded: boolean;
    selected: boolean;
    visuallySelected: boolean;
    indeterminate: boolean;
    expand: () => void;
    select: () => void;
    childTree?: React.ReactNode;
  }) => JSX.Element | null;
};

export function TreeNodeContent<T>(
  props: React.PropsWithChildren<TreeNodeContentProps<T>>
) {
  const onClickExpandTrigger = React.useCallback(() => {
    props.expand(props.treeNode.id);
  }, [props]);
  const onChangeSelectTrigger = React.useCallback(() => {
    props.select(props.treeNode.id);
  }, [props]);

  const selectable = props.selectableIds[props.treeNode.id] ?? false;

  const expanded = props.expandedIds.includes(props.treeNode.id);
  const selected = props.selectedIds.includes(props.treeNode.id);
  const visuallySelected = props.visuallySelectedIds.includes(
    props.treeNode.id
  );
  const indeterminate = props.indeterminateIds.includes(props.treeNode.id);

  const inputId = `${props.treeId}-${props.treeNode.id}`;

  const inputRef = React.useRef<HTMLInputElement>(null);

  React.useEffect(() => {
    if (inputRef.current != null) {
      if (!visuallySelected && indeterminate) {
        inputRef.current.indeterminate = true;
      } else {
        inputRef.current.indeterminate = false;
      }
    }
  }, [indeterminate, visuallySelected]);

  return (
    <>
      {props.renderNodeContent?.({
        treeId: props.treeId,
        treeNode: props.treeNode,
        selectable: selectable,
        expanded: expanded,
        selected: selected,
        visuallySelected: visuallySelected,
        indeterminate: indeterminate,
        expand: onClickExpandTrigger,
        select: onChangeSelectTrigger,
        childTree: props.children,
      }) ?? (
        <>
          <div className="node__inner">
            <input
              ref={inputRef}
              id={inputId}
              type={props.inputType ?? "checkbox"}
              name={props.treeId ?? ""}
              value={props.treeNode.id}
              checked={selected || visuallySelected}
              disabled={!selectable}
              onChange={onChangeSelectTrigger}
            />

            <label
              htmlFor={inputId}
              className={`node__content${
                selected || visuallySelected ? " node__content--selected" : ""
              }${indeterminate ? " node__content--indeterminate" : ""}`}
            >
              {props.treeNode.id}
            </label>

            {props.treeNode.nodes != null && props.treeNode.nodes.length > 0 ? (
              <div className="node__expand-trigger-wrapper">
                <button
                  className={`node__expand-trigger${
                    expanded ? " node__expand-trigger--expanded" : ""
                  }`}
                  type="button"
                  onClick={onClickExpandTrigger}
                >
                  {expanded ? "↑" : "↓"}
                </button>
              </div>
            ) : null}
          </div>

          {props.children != null ? (
            <div
              className="node__children-wrapper"
              style={{ display: expanded ? "block" : "none" }}
            >
              {props.children}
            </div>
          ) : null}

          <style jsx>{`
            .node__inner {
              display: flex;
              padding: 0.5em;
              align-items: center;
            }
            .node__content {
              padding-left: 0.5em;
            }
            .node__content--selected {
              font-weight: bold;
            }
            .node__content--indeterminate {
              font-weight: bold;
            }
            .node__expand-trigger-wrapper {
              margin-left: 0.5em;
            }
            .node__expand-trigger--expanded {
              opacity: 0.5;
            }
            .node__children-wrapper {
              padding-left: 2em;
            }
          `}</style>
        </>
      )}
    </>
  );
}
