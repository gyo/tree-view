import {
  AncestorsObject,
  DescendantsObject,
  Node,
  ParentObject,
  SiblingsObject,
  TreeNode,
} from "components/Tree";

const convertNodeToTreeNode = <T>(nodeData: Node<T>) => {
  const node: TreeNode<T> = {
    id: nodeData.id,
  };
  if (nodeData.body != null) {
    node.body = nodeData.body;
  }
  return node;
};

export const buildTree = <T>(nodes: Node<T>[], parentNode?: TreeNode<T>) => {
  const result: TreeNode<T>[] = [];

  if (parentNode == null) {
    const restNodes: Node<T>[] = [];
    for (const node of nodes) {
      if (node.parentId == null) {
        result.push(convertNodeToTreeNode(node));
      } else {
        restNodes.push(node);
      }
    }

    for (const resultNode of result) {
      buildTree(restNodes, resultNode);
    }
  } else {
    const appendingNodes: Node<T>[] = [];
    const restNodes: Node<T>[] = [];
    for (const node of nodes) {
      if (node.parentId === parentNode?.id) {
        appendingNodes.push(node);
      } else {
        restNodes.push(node);
      }
    }

    for (const appendingNode of appendingNodes) {
      if (parentNode.nodes == null) {
        parentNode.nodes = [];
      }
      const appendingTreeNode = convertNodeToTreeNode(appendingNode);
      parentNode.nodes.push(appendingTreeNode);
      buildTree(restNodes, appendingTreeNode);
    }
  }

  return result;
};

export const getAllNodes = <T>(
  nodes: TreeNode<T>[],
  result: TreeNode<T>["id"][] = []
) => {
  for (const node of nodes) {
    result.push(node.id);
    if (node.nodes != null) {
      getAllNodes(node.nodes, result);
    }
  }

  return result;
};

export const getLeafNodes = <T>(
  nodes: TreeNode<T>[],
  result: TreeNode<T>["id"][] = []
) => {
  for (const node of nodes) {
    if (node.nodes == null) {
      result.push(node.id);
    } else {
      getLeafNodes(node.nodes, result);
    }
  }

  return result;
};

export const getSelectableIds = <T>(
  treeNodes: TreeNode<T>[],
  selectableIds?: Node<T>["id"][],
  onlyLeafNodesSelectable?: boolean
) => {
  let intermediate: Node<T>["id"][];
  if (onlyLeafNodesSelectable === true) {
    intermediate = getLeafNodes(treeNodes);
  } else {
    intermediate = selectableIds ?? getAllNodes(treeNodes);
  }

  const result: Record<Node<T>["id"], boolean | undefined> = {};
  return intermediate.reduce((previous, current) => {
    previous[current] = true;
    return previous;
  }, result);
};

export const getSiblingsObject = <T>(
  node: TreeNode<T>,
  result: SiblingsObject<T> = {}
) => {
  if (node.nodes == null) {
    return result;
  }

  for (const n of node.nodes) {
    result[n.id] = node.nodes
      .filter((node) => {
        return node.id !== n.id;
      })
      .map((n) => {
        return n.id;
      });
    getSiblingsObject(n, result);
  }

  return result;
};

export const getParentObject = <T>(
  nodes: TreeNode<T>[],
  parentId?: TreeNode<T>["id"],
  result: ParentObject<T> = {}
) => {
  for (const node of nodes) {
    if (parentId != null) {
      result[node.id] = parentId;
    }
    if (node.nodes != null) {
      getParentObject(node.nodes, node.id, result);
    }
  }

  return result;
};

export const getAncestorsObject = <T>(
  nodes: TreeNode<T>[],
  parentIds?: TreeNode<T>["id"][],
  parentId?: TreeNode<T>["id"],
  result: AncestorsObject<T> = {}
) => {
  for (const node of nodes) {
    result[node.id] = [...(parentIds ?? [])];
    if (parentId != null) {
      result[node.id]?.push(parentId);
    }
    if (node.nodes != null) {
      getAncestorsObject(node.nodes, result[node.id], node.id, result);
    }
  }

  return result;
};

const getChildIdAll = <T>(node: TreeNode<T>): TreeNode<T>["id"][] => {
  const childIds = node.nodes?.map((n) => n.id) ?? [];
  const descendantIds = node.nodes?.flatMap(getChildIdAll) ?? [];
  return [...childIds, ...descendantIds];
};

export const getDescendantsObject = <T>(
  nodes: TreeNode<T>[],
  result: DescendantsObject<T> = {}
) => {
  for (const node of nodes) {
    result[node.id] = getChildIdAll(node);
    if (node.nodes != null) {
      getDescendantsObject(node.nodes, result);
    }
  }

  return result;
};

export const collectSelectableAncestorNodeIds = <T>(
  currentNodeId: TreeNode<T>["id"],
  selectedIds: TreeNode<T>["id"][],
  siblingsObject: SiblingsObject<T>,
  parentObject: ParentObject<T>,
  result: TreeNode<T>["id"][] = []
) => {
  const siblingsNodeIds = siblingsObject[currentNodeId] ?? [];
  const allSiblingsIsSelected = siblingsNodeIds.every((siblingsNodeId) =>
    selectedIds.includes(siblingsNodeId)
  );
  const parentNodeId = parentObject[currentNodeId];
  if (allSiblingsIsSelected && parentNodeId !== undefined) {
    result.push(parentNodeId);
    collectSelectableAncestorNodeIds(
      parentNodeId,
      selectedIds,
      siblingsObject,
      parentObject,
      result
    );
  }
  return result;
};

export const normalizeExpandedIds = <T>(
  expandedIds: TreeNode<T>["id"][],
  selectedIds: TreeNode<T>["id"][],
  ancestorsObject: AncestorsObject<T>
) => {
  return Array.from(
    new Set([
      ...expandedIds,
      ...selectedIds.flatMap((id) => ancestorsObject[id] ?? []),
    ])
  );
};

export const normalizeSelectedIds = <T>(
  selectedIds: TreeNode<T>["id"][],
  siblingsObject: SiblingsObject<T>,
  parentObject: ParentObject<T>,
  descendantsObject: DescendantsObject<T>,
  multiSelect: boolean,
  selectableIds: Record<TreeNode<T>["id"], boolean | undefined>
) => {
  const filteredSelectedIds = selectedIds.filter((selectedId) => {
    return selectableIds[selectedId] ?? false;
  });

  if (!multiSelect) {
    return selectedIds.slice(0, 1);
  }

  return Array.from(
    new Set(
      [
        ...filteredSelectedIds,
        ...filteredSelectedIds.flatMap((id) => descendantsObject[id] ?? []),
        ...filteredSelectedIds.flatMap((id) =>
          collectSelectableAncestorNodeIds(
            id,
            filteredSelectedIds,
            siblingsObject,
            parentObject
          )
        ),
      ].filter((selectedId) => {
        return selectableIds[selectedId] ?? false;
      })
    )
  );
};

export const normalizeVisuallySelectedIds = <T>(
  selectedIds: TreeNode<T>["id"][],
  siblingsObject: SiblingsObject<T>,
  parentObject: ParentObject<T>,
  descendantsObject: DescendantsObject<T>,
  multiSelect: boolean,
  selectableIds: Record<TreeNode<T>["id"], boolean | undefined>
) => {
  if (!multiSelect) {
    return [];
  }
  return Array.from(
    new Set(
      [
        ...selectedIds,
        ...selectedIds.flatMap((id) => descendantsObject[id] ?? []),
        ...selectedIds.flatMap((id) =>
          collectSelectableAncestorNodeIds(
            id,
            selectedIds,
            siblingsObject,
            parentObject
          )
        ),
      ].filter((selectedId) => {
        return !(selectableIds[selectedId] ?? false);
      })
    )
  );
};

export const normalizeIndeterminateIds = <T>(
  selectedIds: TreeNode<T>["id"][],
  visuallySelectedIds: TreeNode<T>["id"][],
  ancestorsObject: AncestorsObject<T>
) => {
  return Array.from(
    new Set(
      selectedIds
        .flatMap((id) => ancestorsObject[id] ?? [])
        .filter((id) => {
          return !selectedIds.includes(id) && !visuallySelectedIds.includes(id);
        })
    )
  );
};

export const findNode = <T>(
  nodes: TreeNode<T>[],
  nodeId: TreeNode<T>["id"]
): TreeNode<T> | undefined => {
  for (const node of nodes) {
    if (node.id === nodeId) {
      return node;
    } else if (node.nodes != null) {
      const result = findNode(node.nodes, nodeId);
      if (result != null) {
        return result;
      }
    }
  }
};

export const remove = <T>(target: T[], item: T | T[]) => {
  return target.filter((arrayItem) => {
    if (Array.isArray(item)) {
      return !item.includes(arrayItem);
    } else {
      return arrayItem !== item;
    }
  });
};
