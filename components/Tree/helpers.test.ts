import {
  buildTree,
  collectSelectableAncestorNodeIds,
  findNode,
  getAllNodes,
  getAncestorsObject,
  getDescendantsObject,
  getLeafNodes,
  getParentObject,
  getSelectableIds,
  getSiblingsObject,
  normalizeExpandedIds,
  normalizeIndeterminateIds,
  normalizeSelectedIds,
  normalizeVisuallySelectedIds,
  remove,
} from "components/Tree/helpers";

test("buildTree", () => {
  const nodeDataList = [
    { id: "1" },
    { id: "2", body: { label: "Node-2" } },
    { id: "2-1", parentId: "2" },
    { id: "3-2-1", parentId: "3-2" },
    { id: "3-2", parentId: "3" },
    { id: "3-1", parentId: "3", body: { label: "Node-3-1" } },
    { id: "3" },
  ];

  const nodes = [
    { id: "1" },
    { id: "2", body: { label: "Node-2" }, nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [
        { id: "3-2", nodes: [{ id: "3-2-1" }] },
        { id: "3-1", body: { label: "Node-3-1" } },
      ],
    },
  ];

  expect(buildTree(nodeDataList)).toEqual(nodes);
});

test("getAllNodes", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  expect(getAllNodes(nodes)).toEqual([
    "1",
    "2",
    "2-1",
    "3",
    "3-1",
    "3-2",
    "3-2-1",
  ]);
});

test("getLeafNodes", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  expect(getLeafNodes(nodes)).toEqual(["1", "2-1", "3-1", "3-2-1"]);
});

describe("getSelectableIds", () => {
  test("without selectableIds, onlyLeafNodesSelectable", () => {
    const nodes = [
      { id: "1" },
      { id: "2", nodes: [{ id: "2-1" }] },
      {
        id: "3",
        nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
      },
    ];

    expect(getSelectableIds(nodes)).toEqual({
      "1": true,
      "2": true,
      "2-1": true,
      "3": true,
      "3-1": true,
      "3-2": true,
      "3-2-1": true,
    });
  });

  test("with selectableIds", () => {
    const nodes = [
      { id: "1" },
      { id: "2", nodes: [{ id: "2-1" }] },
      {
        id: "3",
        nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
      },
    ];
    const selectableIds = ["2", "3-2", "3-2-1"];

    expect(getSelectableIds(nodes, selectableIds)).toEqual({
      "2": true,
      "3-2": true,
      "3-2-1": true,
    });
  });

  test("with onlyLeafNodesSelectable", () => {
    const nodes = [
      { id: "1" },
      { id: "2", nodes: [{ id: "2-1" }] },
      {
        id: "3",
        nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
      },
    ];

    expect(getSelectableIds(nodes, undefined, true)).toEqual({
      "1": true,
      "2-1": true,
      "3-1": true,
      "3-2-1": true,
    });
  });
});

test("getSiblingsObject", () => {
  const tree = {
    id: "root",
    nodes: [
      { id: "1" },
      { id: "2", nodes: [{ id: "2-1" }] },
      {
        id: "3",
        nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
      },
    ],
  };

  expect(getSiblingsObject(tree)).toEqual({
    "1": ["2", "3"],
    "2": ["1", "3"],
    "2-1": [],
    "3": ["1", "2"],
    "3-1": ["3-2"],
    "3-2": ["3-1"],
    "3-2-1": [],
  });
});

test("getParentObject", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  expect(getParentObject(nodes)).toEqual({
    "1": undefined,
    "2": undefined,
    "2-1": "2",
    "3": undefined,
    "3-1": "3",
    "3-2": "3",
    "3-2-1": "3-2",
  });
});

test("getAncestorsObject", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  expect(getAncestorsObject(nodes)).toEqual({
    "1": [],
    "2": [],
    "2-1": ["2"],
    "3": [],
    "3-1": ["3"],
    "3-2": ["3"],
    "3-2-1": ["3", "3-2"],
  });
});

test("getDescendantsObject", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  expect(getDescendantsObject(nodes)).toEqual({
    "1": [],
    "2": ["2-1"],
    "2-1": [],
    "3": ["3-1", "3-2", "3-2-1"],
    "3-1": [],
    "3-2": ["3-2-1"],
    "3-2-1": [],
  });
});

describe("collectSelectableAncestorNodeIds", () => {
  test("with invalid siblingsObject", () => {
    expect(
      collectSelectableAncestorNodeIds(
        "1-1",
        [],
        {},
        {
          "1": undefined,
          "1-1": "1",
        }
      )
    ).toEqual(["1"]);
  });

  test("without siblings", () => {
    expect(
      collectSelectableAncestorNodeIds(
        "1-1",
        [],
        {
          "1": [],
          "1-1": [],
        },
        {
          "1": undefined,
          "1-1": "1",
        }
      )
    ).toEqual(["1"]);
  });

  test("with unselected siblings", () => {
    expect(
      collectSelectableAncestorNodeIds(
        "1-1",
        [],
        {
          "1": [],
          "1-1": ["1-2"],
          "1-2": ["1-1"],
        },
        {
          "1": undefined,
          "1-1": "1",
        }
      )
    ).toEqual([]);
  });

  test("with selected siblings", () => {
    expect(
      collectSelectableAncestorNodeIds(
        "1-1",
        ["1-2"],
        {
          "1": [],
          "1-1": ["1-2"],
          "1-2": ["1-1"],
        },
        {
          "1": undefined,
          "1-1": "1",
        }
      )
    ).toEqual(["1"]);
  });
});

describe("normalizeExpandedIds", () => {
  test("with invalid ancestorsObject", () => {
    expect(normalizeExpandedIds(["1"], ["3-2-1"], {})).toEqual(["1"]);
  });

  test("with valid ancestorsObject", () => {
    expect(
      normalizeExpandedIds(["1"], ["3-2-1"], {
        "1": [],
        "3": [],
        "3-1": ["3"],
        "3-2": ["3"],
        "3-2-1": ["3", "3-2"],
      })
    ).toEqual(["1", "3", "3-2"]);
  });
});

describe("normalizeSelectedIds", () => {
  test("with invalid descendantsObject", () => {
    expect(
      normalizeSelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {},
        true,
        {
          "1": true,
          "2": true,
          "2-1": true,
          "3": true,
          "3-1": true,
        }
      )
    ).toEqual(["1", "2", "3-1", "3"]);
  });

  test("with valid descendantsObject", () => {
    expect(
      normalizeSelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {
          "1": [],
          "2": ["2-1"],
          "2-1": [],
          "3": ["3-1"],
          "3-1": [],
        },
        true,
        {
          "1": true,
          "2": true,
          "2-1": true,
          "3": true,
          "3-1": true,
        }
      )
    ).toEqual(["1", "2", "3-1", "2-1", "3"]);
  });

  test("with multiSelect false", () => {
    expect(
      normalizeSelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {
          "1": [],
          "2": ["2-1"],
          "2-1": [],
          "3": ["3-1"],
          "3-1": [],
        },
        false,
        {
          "1": true,
          "2": true,
          "2-1": true,
          "3": true,
          "3-1": true,
        }
      )
    ).toEqual(["1"]);
  });

  test("with selectableIds", () => {
    expect(
      normalizeSelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {
          "1": [],
          "2": ["2-1"],
          "2-1": [],
          "3": ["3-1"],
          "3-1": [],
        },
        true,
        {
          "2": true,
          "3-1": true,
        }
      )
    ).toEqual(["2", "3-1"]);
  });
});

describe("normalizeVisuallySelectedIds", () => {
  test("without selectableIds", () => {
    expect(
      normalizeVisuallySelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {},
        true,
        {
          "1": true,
          "2": true,
          "2-1": true,
          "3": true,
          "3-1": true,
        }
      )
    ).toEqual([]);
  });

  test("with valid arguments", () => {
    expect(
      normalizeVisuallySelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {
          "1": [],
          "2": ["2-1"],
          "2-1": [],
          "3": ["3-1"],
          "3-1": [],
        },
        true,
        {
          "2": true,
          "2-1": true,
        }
      )
    ).toEqual(["1", "3-1", "3"]);
  });

  test("with multiSelect false", () => {
    expect(
      normalizeVisuallySelectedIds(
        ["1", "2", "3-1"],
        {
          "1": [],
          "2": [],
          "2-1": [],
          "3": [],
          "3-1": [],
        },
        {
          "1": undefined,
          "2": undefined,
          "2-1": "2",
          "3": undefined,
          "3-1": "3",
        },
        {
          "1": [],
          "2": ["2-1"],
          "2-1": [],
          "3": ["3-1"],
          "3-1": [],
        },
        false,
        {
          "2": true,
          "2-1": true,
        }
      )
    ).toEqual([]);
  });
});

describe("normalizeIndeterminateIds", () => {
  test("with invalid ancestorsObject", () => {
    expect(
      normalizeIndeterminateIds(["2", "2-1", "3-1-1"], ["3-1"], {})
    ).toEqual([]);
  });

  test("with valid ancestorsObject", () => {
    expect(
      normalizeIndeterminateIds(["2", "2-1", "3-1-1"], ["3-1"], {
        "1": [],
        "1-1": ["1"],
        "2": [],
        "2-1": ["2"],
        "3": [],
        "3-1": ["3"],
        "3-1-1": ["3", "3-1"],
      })
    ).toEqual(["3"]);
  });

  test("with multiSelect false", () => {
    expect(
      normalizeIndeterminateIds(["2-1"], [], {
        "2": [],
        "2-1": ["2"],
      })
    ).toEqual(["2"]);
  });
});

describe("findNode", () => {
  const nodes = [
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1" }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ];

  test("found shallow node", () => {
    expect(findNode(nodes, "2")).toBe(nodes[1]);
  });

  test("found deep node", () => {
    /* eslint-disable-next-line -- @typescript-eslint/no-explicit-any */
    expect(findNode(nodes, "3-2")).toBe((nodes[2] as any).nodes[1]);
  });

  test("not found", () => {
    expect(findNode(nodes, "4")).toBe(undefined);
  });
});

describe("remove", () => {
  test("one item", () => {
    expect(remove(["1", "2"], "1")).toEqual(["2"]);
  });

  test("some items", () => {
    expect(remove(["1", "2", "3"], ["1", "3"])).toEqual(["2"]);
  });
});
