export type Node<T> = {
  id: string;
  parentId?: string;
  body?: T;
};

export type TreeNode<T> = {
  id: Node<T>["id"];
  body?: Node<T>["body"];
  nodes?: TreeNode<T>[];
};

export type SiblingsObject<T> = Record<
  Node<T>["id"],
  Node<T>["id"][] | undefined
>;
export type ParentObject<T> = Record<Node<T>["id"], Node<T>["id"] | undefined>;
export type AncestorsObject<T> = Record<
  Node<T>["id"],
  Node<T>["id"][] | undefined
>;
export type DescendantsObject<T> = Record<
  Node<T>["id"],
  Node<T>["id"][] | undefined
>;
