import { Node, useTree } from "components/Tree";

import { act, renderHook } from "@testing-library/react-hooks";

const testNodes: Node<unknown>[] = [
  { id: "1" },
  { id: "2" },
  { id: "2-1", parentId: "2" },
  { id: "3" },
  { id: "3-1", parentId: "3" },
  { id: "3-2", parentId: "3" },
  { id: "3-2-1", parentId: "3-2" },
];

test("without options", () => {
  const { result } = renderHook(() => useTree(testNodes));

  expect(result.current.expandedIds.sort()).toEqual([]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual(["2", "2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("3");
  });

  act(() => {
    result.current.select("3-2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual([
    "2",
    "2-1",
    "3-2",
    "3-2-1",
  ]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual(["3"]);

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual(["3-2", "3-2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual(["3"]);

  act(() => {
    result.current.select("3-2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["3"]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);
});

test("with multiSelect false", () => {
  const { result } = renderHook(() =>
    useTree(testNodes, {
      multiSelect: false,
    })
  );

  act(() => {
    result.current.expand("2");
  });

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual(["2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual(["2"]);

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("3");
  });

  act(() => {
    result.current.select("3-2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual(["3-2"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual(["3"]);
});

test("with selectableIds", () => {
  const { result } = renderHook(() =>
    useTree(testNodes, {
      selectableIds: ["2", "2-1", "3-2"],
    })
  );

  act(() => {
    result.current.select("1");
  });

  expect(result.current.expandedIds.sort()).toEqual([]);
  expect(result.current.selectedIds.sort()).toEqual([]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("2");
  });

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual(["2", "2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual([]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("3");
  });

  act(() => {
    result.current.select("3-2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual(["2", "2-1", "3-2"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual(["3-2-1"]);
  expect(result.current.indeterminateIds.sort()).toEqual(["3"]);
});

test("with onlyLeafNodesSelectable", () => {
  const { result } = renderHook(() =>
    useTree(testNodes, {
      onlyLeafNodesSelectable: true,
    })
  );

  act(() => {
    result.current.expand("2");
  });

  act(() => {
    result.current.select("2-1");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2"]);
  expect(result.current.selectedIds.sort()).toEqual(["2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual(["2"]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);

  act(() => {
    result.current.expand("3");
  });

  act(() => {
    result.current.select("3-2");
  });

  expect(result.current.expandedIds.sort()).toEqual(["2", "3"]);
  expect(result.current.selectedIds.sort()).toEqual(["2-1"]);
  expect(result.current.visuallySelectedIds.sort()).toEqual(["2"]);
  expect(result.current.indeterminateIds.sort()).toEqual([]);
});

test("with onExpand append", () => {
  const { result } = renderHook(() =>
    useTree(testNodes, {
      onExpand: (
        currentNode: Node<unknown>,
        direction: "open" | "close",
        append: (appendingNodes: Node<unknown>[]) => void
      ) => {
        if (direction === "open" && currentNode.id === "2") {
          append([{ id: "new", parentId: "2-1" }]);
        }
      },
    })
  );

  act(() => {
    result.current.expand("2");
  });

  expect(result.current.treeNodes).toEqual([
    { id: "1" },
    { id: "2", nodes: [{ id: "2-1", nodes: [{ id: "new" }] }] },
    {
      id: "3",
      nodes: [{ id: "3-1" }, { id: "3-2", nodes: [{ id: "3-2-1" }] }],
    },
  ]);
});
