import {
  TreeNodeContent,
  TreeNodeContentProps,
} from "components/Tree/components";
import React from "react";

export * from "components/Tree/components";
export * from "components/Tree/hooks";
export * from "components/Tree/types";

type Props<T = undefined> = {
  treeId?: TreeNodeContentProps<T>["treeId"];
  inputType?: TreeNodeContentProps<T>["inputType"];
  selectableIds: TreeNodeContentProps<T>["selectableIds"];
  multiSelect: TreeNodeContentProps<T>["multiSelect"];
  treeNodes: TreeNodeContentProps<T>["treeNode"][];
  expandedIds: TreeNodeContentProps<T>["expandedIds"];
  selectedIds: TreeNodeContentProps<T>["selectedIds"];
  visuallySelectedIds: TreeNodeContentProps<T>["visuallySelectedIds"];
  indeterminateIds: TreeNodeContentProps<T>["indeterminateIds"];
  expand: TreeNodeContentProps<T>["expand"];
  select: TreeNodeContentProps<T>["select"];
  renderNodeContent?: TreeNodeContentProps<T>["renderNodeContent"];
};

export function Tree<T>(props: React.PropsWithChildren<Props<T>>) {
  const [treeId, setTreeId] = React.useState("");
  React.useEffect(() => {
    setTreeId(props.treeId ?? Math.random().toString(32).substring(2));
  }, [props.treeId, setTreeId]);

  return (
    <>
      {props.treeNodes.map((currentNode) => {
        return (
          <TreeNodeContent
            key={currentNode.id}
            treeId={treeId}
            inputType={props.inputType}
            selectableIds={props.selectableIds}
            multiSelect={props.multiSelect}
            treeNode={currentNode}
            expandedIds={props.expandedIds}
            selectedIds={props.selectedIds}
            visuallySelectedIds={props.visuallySelectedIds}
            indeterminateIds={props.indeterminateIds}
            expand={props.expand}
            select={props.select}
            renderNodeContent={props.renderNodeContent}
          >
            {currentNode.nodes != null && currentNode.nodes.length > 0 ? (
              <Tree
                treeId={treeId}
                inputType={props.inputType}
                selectableIds={props.selectableIds}
                multiSelect={props.multiSelect}
                treeNodes={currentNode.nodes}
                expandedIds={props.expandedIds}
                selectedIds={props.selectedIds}
                visuallySelectedIds={props.visuallySelectedIds}
                indeterminateIds={props.indeterminateIds}
                expand={props.expand}
                select={props.select}
                renderNodeContent={props.renderNodeContent}
              ></Tree>
            ) : null}
          </TreeNodeContent>
        );
      })}
    </>
  );
}
