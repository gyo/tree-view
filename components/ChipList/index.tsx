import React from "react";

type Props = {
  items: string[];
};

export const ChipList: React.FC<Props> = (props) => {
  return (
    <>
      {props.items.sort().map((item) => {
        return <span key={item}>{item}</span>;
      })}

      <style jsx>{`
        span {
          display: inline-block;
          background: lightgray;
          padding: 0 0.25em;
          margin-right: 0.5em;
        }
      `}</style>
    </>
  );
};
