import { Node, TreeNode } from "components/Tree";

export type SampleNode = Node<unknown>;

export type SampleTreeNode = TreeNode<unknown>;

export const sampleNodes: SampleNode[] = [
  { id: "1" },
  { id: "2" },
  { id: "2-1", parentId: "2" },
  { id: "2-2", parentId: "2" },
];
