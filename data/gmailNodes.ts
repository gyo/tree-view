import { Node, TreeNode } from "components/Tree";

export type SampleNodeBody = {
  icon?: string;
  label: string;
  path: string;
  count: number;
};

export type SampleNode = Node<SampleNodeBody>;

export type SampleTreeNode = TreeNode<SampleNodeBody>;

export const sampleNodes: SampleNode[] = [
  {
    id: "all-mail",
    body: { icon: "Mail", label: "All Mail", path: "/all-mail", count: 0 },
  },
  {
    id: "trash",
    body: { icon: "Delete", label: "Trash", path: "/trash", count: 0 },
  },
  {
    id: "categories",
    body: { label: "Categories", path: "/categories", count: 0 },
  },
  {
    id: "social",
    body: {
      icon: "SupervisorAccount",
      label: "Social",
      path: "/social",
      count: 90,
    },
    parentId: "categories",
  },
  {
    id: "updates",
    body: { icon: "Info", label: "Updates", path: "/updates", count: 2294 },
    parentId: "categories",
  },
  {
    id: "forums",
    body: {
      icon: "QuestionAnswer",
      label: "Forums",
      path: "/forums",
      count: 3566,
    },
    parentId: "categories",
  },
  {
    id: "promotions",
    body: {
      icon: "LocalOffer",
      label: "Promotions",
      path: "/promotions",
      count: 733,
    },
    parentId: "categories",
  },
  { id: "history", body: { label: "History", path: "/history", count: 0 } },
];
