import { Node, TreeNode } from "components/Tree";

export type SampleNode = Node<unknown>;

export type SampleTreeNode = TreeNode<unknown>;

export const sampleNodes: SampleNode[] = [
  { id: "1" },
  { id: "2" },
  { id: "2-1", parentId: "2" },
  { id: "3" },
  { id: "3-1", parentId: "3" },
  { id: "3-2", parentId: "3" },
  { id: "3-2-1", parentId: "3-2" },
  { id: "4" },
  { id: "4-1", parentId: "4" },
  { id: "4-2", parentId: "4" },
  { id: "4-2-1", parentId: "4-2" },
  { id: "4-3", parentId: "4" },
  { id: "4-3-1", parentId: "4-3" },
  { id: "4-3-2", parentId: "4-3" },
  { id: "4-3-2-1", parentId: "4-3-2" },
  { id: "5" },
  { id: "5-1", parentId: "5" },
  { id: "5-2", parentId: "5" },
  { id: "5-2-1", parentId: "5-2" },
  { id: "5-3", parentId: "5" },
  { id: "5-3-1", parentId: "5-3" },
  { id: "5-3-2", parentId: "5-3" },
  { id: "5-3-2-1", parentId: "5-3-2" },
  { id: "5-4", parentId: "5" },
  { id: "5-4-1", parentId: "5-4" },
  { id: "5-4-2", parentId: "5-4" },
  { id: "5-4-2-1", parentId: "5-4-2" },
  { id: "5-4-3", parentId: "5-4" },
  { id: "5-4-3-1", parentId: "5-4-3" },
  { id: "5-4-3-2", parentId: "5-4-3" },
  { id: "5-4-3-2-1", parentId: "5-4-3-2" },
];
