import { Node, TreeNode } from "components/Tree";

export type SampleNodeBody = {
  label: string;
};

export type SampleNode = Node<SampleNodeBody>;

export type SampleTreeNode = TreeNode<SampleNodeBody>;

export const sampleNodes: SampleNode[] = [
  { id: "1", body: { label: "Label 1" } },
  { id: "2", body: { label: "Label 2" } },
  { id: "2-1", parentId: "2", body: { label: "Label 2-1" } },
  { id: "3", body: { label: "Label 3" } },
  { id: "3-1", parentId: "3", body: { label: "Label 3-1" } },
  { id: "3-2", parentId: "3", body: { label: "Label 3-2" } },
  { id: "3-2-1", parentId: "3-2", body: { label: "Label 3-2-1" } },
  { id: "4", body: { label: "Label 4" } },
  { id: "4-1", parentId: "4", body: { label: "Label 4-1" } },
  { id: "4-2", parentId: "4", body: { label: "Label 4-2" } },
  { id: "4-2-1", parentId: "4-2", body: { label: "Label 4-2-1" } },
  { id: "4-3", parentId: "4", body: { label: "Label 4-3" } },
  { id: "4-3-1", parentId: "4-3", body: { label: "Label 4-3-1" } },
  { id: "4-3-2", parentId: "4-3", body: { label: "Label 4-3-2" } },
  { id: "4-3-2-1", parentId: "4-3-2", body: { label: "Label 4-3-2-1" } },
  { id: "5", body: { label: "Label 5" } },
  { id: "5-1", parentId: "5", body: { label: "Label 5-1" } },
  { id: "5-2", parentId: "5", body: { label: "Label 5-2" } },
  { id: "5-2-1", parentId: "5-2", body: { label: "Label 5-2-1" } },
  { id: "5-3", parentId: "5", body: { label: "Label 5-3" } },
  { id: "5-3-1", parentId: "5-3", body: { label: "Label 5-3-1" } },
  { id: "5-3-2", parentId: "5-3", body: { label: "Label 5-3-2" } },
  { id: "5-3-2-1", parentId: "5-3-2", body: { label: "Label 5-3-2-1" } },
  { id: "5-4", parentId: "5", body: { label: "Label 5-4" } },
  { id: "5-4-1", parentId: "5-4", body: { label: "Label 5-4-1" } },
  { id: "5-4-2", parentId: "5-4", body: { label: "Label 5-4-2" } },
  { id: "5-4-2-1", parentId: "5-4-2", body: { label: "Label 5-4-2-1" } },
  { id: "5-4-3", parentId: "5-4", body: { label: "Label 5-4-3" } },
  { id: "5-4-3-1", parentId: "5-4-3", body: { label: "Label 5-4-3-1" } },
  { id: "5-4-3-2", parentId: "5-4-3", body: { label: "Label 5-4-3-2" } },
  { id: "5-4-3-2-1", parentId: "5-4-3-2", body: { label: "Label 5-4-3-2-1" } },
];
