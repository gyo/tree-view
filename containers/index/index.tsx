import { NextPage } from "next";
import Link from "next/link";
import React from "react";

export const Container: NextPage = () => {
  return (
    <main>
      <h1>Index</h1>

      <ul>
        <li>
          <div>Default</div>
          <ul>
            <li>
              <Link href="tree/default">
                <a href="tree/default">Default</a>
              </Link>
            </li>
            <li>
              <Link href="tree/initial-value">
                <a href="tree/initial-value">With initial value</a>
              </Link>
            </li>
            <li>
              <Link href="tree/restricted-selectable">
                <a href="tree/restricted-selectable">Restricted Selectable</a>
              </Link>
            </li>
            <li>
              <Link href="tree/only-leaf-nodes-selectable">
                <a href="tree/only-leaf-nodes-selectable">
                  Only leaf nodes selectable
                </a>
              </Link>
            </li>
            <li>
              <Link href="tree/single-select">
                <a href="tree/single-select">Single Select</a>
              </Link>
            </li>
            <li>
              <Link href="tree/single-select-radio">
                <a href="tree/single-select-radio">Single Select Radio</a>
              </Link>
            </li>
          </ul>
        </li>
        <li>
          <div>Custom</div>
          <ul>
            <li>
              <Link href="tree/custom-single-select">
                <a href="tree/custom-single-select">Custom Single Select</a>
              </Link>
            </li>
            <li>
              <Link href="tree/custom-multi-select">
                <a href="tree/custom-multi-select">Custom Multi Select</a>
              </Link>
            </li>
            <li>
              <Link href="tree/custom">
                <a href="tree/custom">Custom</a>
              </Link>
            </li>
            <li>
              <Link href="tree/gmail-like">
                <a href="tree/gmail-like">Gmail Like</a>
              </Link>
            </li>
          </ul>
        </li>
        <li>
          <Link href="tree/append-nodes">
            <a href="tree/append-nodes">Append Nodes</a>
          </Link>
        </li>
        <li>
          <Link href="tree/large-tree">
            <a href="tree/large-tree">Large Tree</a>
          </Link>
        </li>
      </ul>
    </main>
  );
};
