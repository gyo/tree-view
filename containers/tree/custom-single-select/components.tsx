import { SampleTreeNode } from "data/nodesWithLabel";
import React from "react";

import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";

type Props = {
  treeId: string;
  treeNode: SampleTreeNode;
  expanded: boolean;
  selected: boolean;
  visuallySelected: boolean;
  indeterminate: boolean;
  expand: () => void;
  select: () => void;
  childTree?: React.ReactNode;
};

export const TreeContent: React.FC<Props> = (props) => {
  if (props.childTree != null) {
    return (
      <TreeContentWithChildTree
        treeNode={props.treeNode}
        expanded={props.expanded}
        expand={props.expand}
        visuallySelected={props.visuallySelected}
        indeterminate={props.indeterminate}
        childTree={props.childTree}
      />
    );
  } else {
    return (
      <TreeContentWithoutChildTree
        treeId={props.treeId}
        treeNode={props.treeNode}
        selected={props.selected}
        visuallySelected={props.visuallySelected}
        indeterminate={props.indeterminate}
        select={props.select}
      />
    );
  }
};

type TreeContentWithChildTreeProps = {
  treeNode: Props["treeNode"];
  expanded: Props["expanded"];
  visuallySelected: boolean;
  indeterminate: boolean;
  expand: Props["expand"];
  childTree: Props["childTree"];
};

const TreeContentWithChildTree: React.FC<TreeContentWithChildTreeProps> = (
  props
) => {
  const handleClickTreeContent = React.useCallback(() => {
    props.expand();
  }, [props]);

  return (
    <>
      <div>
        <button className="tree-content" onClick={handleClickTreeContent}>
          <span className="tree-content__expand-trigger-wrapper">
            <span className="tree-content__expand-trigger">
              {props.expanded ? (
                <ExpandMoreIcon fontSize="small" />
              ) : (
                <ChevronRightIcon fontSize="small" />
              )}
            </span>
          </span>

          <div
            className={`tree-content__label${
              props.indeterminate || props.visuallySelected
                ? " tree-content__label--bold"
                : ""
            }`}
          >
            {props.treeNode.body?.label}
          </div>
        </button>

        <div
          className={`children-wrapper${
            props.expanded ? " children-wrapper--expanded" : ""
          }`}
        >
          {props.childTree}
        </div>
      </div>

      <style jsx>{`
        button {
          border-style: none;
          padding: 0;
          background-color: transparent;
          font: inherit;
          text-transform: none;
          color: inherit;
          cursor: pointer;
          overflow: visible;
        }
        .tree-content {
          display: inline-flex;
          align-items: center;
          padding: 0.25em;
          cursor: pointer;
        }
        .tree-content:hover,
        .tree-content:focus,
        .tree-content:focus-within {
          background: whitesmoke;
        }
        .tree-content__expand-trigger-wrapper {
          margin-right: 0.25em;
        }
        .tree-content__expand-trigger {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 1.5em;
        }
        .tree-content__label--bold {
          font-weight: bold;
        }
        .children-wrapper {
          display: none;
          padding-left: 2em;
        }
        .children-wrapper--expanded {
          display: block;
        }
      `}</style>
    </>
  );
};

type TreeContentWithoutChildTreeProps = {
  treeId: Props["treeId"];
  treeNode: Props["treeNode"];
  selected: Props["selected"];
  visuallySelected: boolean;
  indeterminate: boolean;
  select: Props["select"];
};

const TreeContentWithoutChildTree: React.FC<TreeContentWithoutChildTreeProps> = (
  props
) => {
  const handleClickTreeContent = React.useCallback(() => {
    props.select();
  }, [props]);

  return (
    <>
      <div>
        <label className="tree-content">
          <span className="tree-content__select-trigger-wrapper">
            <span className="tree-content__select-trigger">
              {props.selected ? (
                <RadioButtonCheckedIcon fontSize="small" />
              ) : (
                <RadioButtonUncheckedIcon fontSize="small" />
              )}

              <input
                type="radio"
                name={props.treeId}
                value={props.treeNode.id}
                onClick={handleClickTreeContent}
              />
            </span>
          </span>

          <div
            className={`tree-content__label${
              props.selected || props.indeterminate || props.visuallySelected
                ? " tree-content__label--bold"
                : ""
            }`}
          >
            {props.treeNode.body?.label}
          </div>
        </label>
      </div>

      <style jsx>{`
        button {
          border-style: none;
          padding: 0;
          background-color: transparent;
          font: inherit;
          text-transform: none;
          color: inherit;
          cursor: pointer;
          overflow: visible;
        }
        input {
          position: absolute;
          white-space: nowrap;
          width: 1px;
          height: 1px;
          overflow: hidden;
          border: 0;
          padding: 0;
          clip: rect(0 0 0 0);
          clip-path: inset(50%);
          margin: -1px;
        }
        .tree-content {
          display: inline-flex;
          align-items: center;
          padding: 0.25em;
          cursor: pointer;
        }
        .tree-content:hover,
        .tree-content:focus,
        .tree-content:focus-within {
          background: whitesmoke;
        }
        .tree-content__select-trigger-wrapper {
          margin-right: 0.25em;
        }
        .tree-content__select-trigger {
          display: flex;
          justify-content: center;
          align-items: center;
          width: 1.5em;
        }
        .tree-content__label--bold {
          font-weight: bold;
        }
      `}</style>
    </>
  );
};
