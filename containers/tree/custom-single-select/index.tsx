import { ChipList } from "components/ChipList";
import { Tree, TreeNodeContentProps, useTree } from "components/Tree";
import { TreeContent } from "containers/tree/custom-single-select/components";
import { SampleNodeBody, sampleNodes } from "data/nodesWithLabel";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const renderNodeContent: TreeNodeContentProps<
  SampleNodeBody
>["renderNodeContent"] = (params) => {
  return (
    <TreeContent
      treeId={params.treeId}
      treeNode={params.treeNode}
      expanded={params.expanded}
      selected={params.selected}
      visuallySelected={params.visuallySelected}
      indeterminate={params.indeterminate}
      expand={params.expand}
      select={params.select}
      childTree={params.childTree}
    />
  );
};

export const Container: NextPage = () => {
  const treeProps = useTree(sampleNodes, {
    onlyLeafNodesSelectable: true,
    multiSelect: false,
  });

  return (
    <>
      <h1>Custom Single Select</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <Tree {...treeProps} renderNodeContent={renderNodeContent}></Tree>
      </section>
    </>
  );
};
