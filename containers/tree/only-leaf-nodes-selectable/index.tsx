import { ChipList } from "components/ChipList";
import { Tree, useTree } from "components/Tree";
import { SampleNode, sampleNodes } from "data/nodes";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const defaultSelectedIds: SampleNode["id"][] = ["5-4-3-2-1"];

export const Container: NextPage = () => {
  const treeProps = useTree(sampleNodes, {
    defaultSelectedIds,
    onlyLeafNodesSelectable: true,
  });

  return (
    <>
      <h1>Only leaf nodes selectable</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <p>Initial selected IDs: {defaultSelectedIds.sort().join(", ")}</p>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <Tree {...treeProps}></Tree>
      </section>
    </>
  );
};
