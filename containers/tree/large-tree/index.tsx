import { ChipList } from "components/ChipList";
import { Tree, useTree } from "components/Tree";
import { sampleNodes } from "data/largeNodes";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

export const Container: NextPage = () => {
  const treeProps = useTree(sampleNodes);

  return (
    <>
      <h1>Large Tree</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <p>Node count: {sampleNodes.length}</p>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <Tree {...treeProps}></Tree>
      </section>
    </>
  );
};
