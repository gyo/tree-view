import { getIconComponent } from "containers/tree/gmail-like/helpers";
import { SampleTreeNode } from "data/gmailNodes";
import React from "react";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowRight from "@material-ui/icons/ArrowRight";

type Props = {
  treeNode: SampleTreeNode;
  expanded: boolean;
  selected: boolean;
  expand: () => void;
  select: () => void;
};

export const TreeContent: React.FC<Props> = (props) => {
  const handleOnExpand = React.useCallback(() => {
    props.expand();
  }, [props]);
  const handleOnSelect = React.useCallback(
    (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      e.preventDefault();
      props.select();
    },
    [props]
  );

  return props.treeNode.body != null ? (
    <>
      <div className="tree-content">
        <div className="tree-content__expand-icon">
          {props.treeNode.nodes != null && props.treeNode.nodes.length > 0 ? (
            <button
              className="tree-content__expand-button"
              onClick={handleOnExpand}
            >
              {props.expanded ? <ArrowDropDown /> : <ArrowRight />}
            </button>
          ) : null}
        </div>

        <a
          className={`tree-content__selectable-area${
            props.selected ? " tree-content__selectable-area--selected" : ""
          }`}
          href={props.treeNode.body.path}
          onClick={handleOnSelect}
        >
          <div className="tree-content__icon">
            {getIconComponent(props.treeNode.body.icon)}
          </div>

          <div className="tree-content__label">{props.treeNode.body.label}</div>

          <div className="tree-content__count">
            {props.treeNode.body.count !== 0
              ? `${props.treeNode.body.count}`
              : ""}
          </div>
        </a>
      </div>

      {props.children != null ? (
        <div
          className="tree-children"
          style={{ display: props.expanded ? "block" : "none" }}
        >
          {props.children}
        </div>
      ) : null}

      <style jsx>{`
        .tree-content {
          display: flex;
        }
        .tree-content__expand-icon {
          flex-shrink: 0;
          display: flex;
          width: 2em;
        }
        .tree-content__expand-button {
          box-sizing: inherit;
          margin: 0;
          padding: 0;
          border-style: none;
          width: 100%;
          height: 100%;
          background-repeat: no-repeat;
          background-color: transparent;
          font: inherit;
          color: inherit;
          text-transform: none;
          overflow: visible;
          cursor: pointer;
          -webkit-appearance: button;
        }
        .tree-content__selectable-area {
          display: flex;
          padding: 0.5em;
          width: 100%;
          border-radius: 0 20px 20px 0;
          color: inherit;
          text-decoration: inherit;
        }
        .tree-content__selectable-area:hover {
          background: whitesmoke;
        }
        .tree-content__selectable-area:focus {
          background: whitesmoke;
        }
        .tree-content__selectable-area.tree-content__selectable-area--selected {
          background: lightgray;
        }
        .tree-content__icon {
          flex-shrink: 0;
          width: 2em;
        }
        .tree-content__label {
          flex-grow: 1;
          flex-wrap: wrap;
        }
        .tree-content__count {
          flex-shrink: 0;
        }
        .tree-children {
          padding-left: 2em;
        }
      `}</style>
    </>
  ) : null;
};
