import { ChipList } from "components/ChipList";
import { Tree, TreeNodeContentProps, useTree } from "components/Tree";
import { TreeContent } from "containers/tree/gmail-like/components";
import { SampleNodeBody, sampleNodes } from "data/gmailNodes";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const renderNodeContent: TreeNodeContentProps<
  SampleNodeBody
>["renderNodeContent"] = (params) => (
  <TreeContent
    treeNode={params.treeNode}
    expanded={params.expanded}
    selected={params.selected}
    expand={params.expand}
    select={params.select}
  >
    {params.childTree}
  </TreeContent>
);

export const Container: NextPage = () => {
  const treeProps = useTree<SampleNodeBody>(sampleNodes, {
    defaultExpandedIds: ["categories"],
    defaultSelectedIds: ["all-mail"],
    multiSelect: false,
  });

  return (
    <>
      <h1>Gmail Like</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <div className="tree-wrapper">
          <Tree<SampleNodeBody>
            {...treeProps}
            renderNodeContent={renderNodeContent}
          ></Tree>
        </div>
      </section>

      <style jsx global>{`
        svg {
          vertical-align: middle;
        }

        .tree-wrapper {
          max-width: 360px;
        }
      `}</style>
    </>
  );
};
