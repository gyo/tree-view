import React from "react";

import Delete from "@material-ui/icons/Delete";
import Info from "@material-ui/icons/Info";
import Label from "@material-ui/icons/Label";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Mail from "@material-ui/icons/Mail";
import QuestionAnswer from "@material-ui/icons/QuestionAnswer";
import SupervisorAccount from "@material-ui/icons/SupervisorAccount";

export const getIconComponent = (iconType?: string) => {
  if (iconType === "Delete") {
    return <Delete />;
  }
  if (iconType === "Info") {
    return <Info />;
  }
  if (iconType === "Label") {
    return <Label />;
  }
  if (iconType === "LocalOffer") {
    return <LocalOffer />;
  }
  if (iconType === "Mail") {
    return <Mail />;
  }
  if (iconType === "QuestionAnswer") {
    return <QuestionAnswer />;
  }
  if (iconType === "SupervisorAccount") {
    return <SupervisorAccount />;
  }
  return <Label />;
};
