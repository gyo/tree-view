import { ChipList } from "components/ChipList";
import { Tree, TreeNodeContentProps, useTree } from "components/Tree";
import { TreeContent } from "containers/tree/custom/components";
import {
  SampleNodeBody,
  sampleNodes,
  SampleTreeNode,
} from "data/nodesWithLabel";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const defaultExpandedIds: SampleTreeNode["id"][] = ["2"];
const defaultSelectedIds: SampleTreeNode["id"][] = ["3-2", "5-4-3-2-1"];

const renderNodeContent: TreeNodeContentProps<
  SampleNodeBody
>["renderNodeContent"] = (params) => {
  return (
    <TreeContent
      treeId={params.treeId}
      treeNode={params.treeNode}
      expanded={params.expanded}
      selected={params.selected}
      indeterminate={params.indeterminate}
      expand={params.expand}
      select={params.select}
    >
      {params.childTree}
    </TreeContent>
  );
};

export const Container: NextPage = () => {
  const treeProps = useTree<SampleNodeBody>(sampleNodes, {
    defaultExpandedIds,
    defaultSelectedIds,
  });

  return (
    <>
      <h1>Custom</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <p>Initial expanded IDs: {defaultExpandedIds.sort().join(", ")}</p>
      <p>Initial selected IDs: {defaultSelectedIds.sort().join(", ")}</p>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <Tree<SampleNodeBody>
          {...treeProps}
          treeId="my-tree"
          renderNodeContent={renderNodeContent}
        ></Tree>
      </section>

      <style jsx global>{`
        svg {
          vertical-align: middle;
        }
      `}</style>
    </>
  );
};
