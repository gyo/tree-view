import { SampleTreeNode } from "data/nodesWithLabel";
import React from "react";

import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";

type Props = {
  treeId: string;
  treeNode: SampleTreeNode;
  expanded: boolean;
  selected: boolean;
  indeterminate: boolean;
  expand: () => void;
  select: () => void;
};

export const TreeContent: React.FC<Props> = (props) => {
  return (
    <>
      <div className="wrapper">
        <div className="expand-button-area">
          {props.treeNode.nodes?.length ?? 0 > 0 ? (
            <div
              className="expand-button"
              role="button"
              tabIndex={0}
              onClick={props.expand}
              onKeyPress={props.expand}
            >
              {props.expanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </div>
          ) : null}
        </div>

        <div
          className={`select${props.selected ? " selected" : ""}${
            props.indeterminate ? " indeterminate" : ""
          }`}
          role="button"
          tabIndex={0}
          onClick={props.select}
          onKeyPress={props.select}
        >
          {props.selected ? (
            <CheckBoxIcon />
          ) : props.indeterminate ? (
            <IndeterminateCheckBoxIcon />
          ) : (
            <CheckBoxOutlineBlankIcon />
          )}
        </div>

        <input
          type="hidden"
          name={props.treeId}
          value={props.selected ? props.treeNode.id : ""}
        />

        <div>{props.treeNode.body?.label}</div>
      </div>

      {props.children != null ? (
        <div
          className="children"
          style={{ display: props.expanded ? "block" : "none" }}
        >
          {props.children}
        </div>
      ) : null}

      <style jsx>{`
        .wrapper {
          display: flex;
          align-items: center;
          padding: 0.5em 0.5em 0.5em 0;
        }
        .expand-button-area {
          width: 2em;
        }
        .expand-button {
          display: inline-block;
          width: 1em;
          cursor: pointer;
        }
        .select {
          margin-right: 0.5em;
          cursor: pointer;
        }
        .selected {
          color: black;
        }
        .indeterminate {
          color: gray;
        }
        .children {
          padding-left: 2em;
        }
      `}</style>
    </>
  );
};
