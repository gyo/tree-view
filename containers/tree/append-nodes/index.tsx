import { ChipList } from "components/ChipList";
import { OnExpand, Tree, useTree } from "components/Tree";
import { fetchNodes } from "containers/tree/append-nodes/helpers";
import { sampleNodes, SampleTreeNode } from "data/smallNodes";
import { NextPage } from "next";
import Link from "next/link";
import React from "react";

const onExpand: OnExpand<unknown> = (currentNode, direction, append) => {
  const asyncMain = async () => {
    if (currentNode.nodes == null || direction == "close") {
      return;
    }

    const fetchingNodeIds = currentNode.nodes.reduce<SampleTreeNode["id"][]>(
      (accumulator, node) => {
        if (node.nodes == null) {
          accumulator.push(node.id);
        }
        return accumulator;
      },
      []
    );

    if (fetchingNodeIds.length === 0) {
      return;
    }

    const fetchedNodes = await fetchNodes(fetchingNodeIds);
    append(fetchedNodes);
  };
  asyncMain();
};

export const Container: NextPage = () => {
  const treeProps = useTree(sampleNodes, {
    onExpand,
  });

  return (
    <>
      <h1>Append Nodes</h1>

      <Link href="/">
        <a href="/">Index</a>
      </Link>

      <section>
        <h2>State</h2>
        <div>
          Expanded: ({treeProps.expandedIds.length}){" "}
          <ChipList items={treeProps.expandedIds} />
        </div>
        <div>
          Selected: ({treeProps.selectedIds.length}){" "}
          <ChipList items={treeProps.selectedIds} />
        </div>
        <div>
          Visually Selected: ({treeProps.visuallySelectedIds.length}){" "}
          <ChipList items={treeProps.visuallySelectedIds} />
        </div>
        <div>
          Indeterminate: ({treeProps.indeterminateIds.length}){" "}
          <ChipList items={treeProps.indeterminateIds} />
        </div>
      </section>

      <section>
        <h2>Tree</h2>

        <Tree {...treeProps}></Tree>
      </section>
    </>
  );
};
