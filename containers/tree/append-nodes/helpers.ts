const sleep = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, 500);
  });
};

export const fetchNodes = async (nodeIds: string[]) => {
  await sleep();
  console.log("fetchNodes! nodeIds: ", nodeIds);
  return nodeIds.reduce<Record<"id" | "parentId", string>[]>(
    (accumulator, currentId) => {
      accumulator.push({
        id: `${currentId}-${Math.random().toString(32).substring(2, 6)}`,
        parentId: currentId,
      });
      accumulator.push({
        id: `${currentId}-${Math.random().toString(32).substring(2, 6)}`,
        parentId: currentId,
      });
      return accumulator;
    },
    []
  );
};
