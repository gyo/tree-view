const fs = require("fs");

const options = {
  siblingsCount: 5,
  maxDepth: 4,
};

const siblingsCount = options.siblingsCount + 1;
const maxDepth = options.maxDepth - 1;

const generateTreeJson = (nodes = [], layer = 0, parentId) => {
  for (let i = 1; i < siblingsCount; i++) {
    let id = "";
    if (parentId != null) {
      id = `${parentId}-${i}`;
    } else {
      id = `${i}`;
    }

    nodes.push({ id, parentId });
    if (layer < maxDepth) {
      generateTreeJson(nodes, layer + 1, id);
    }
  }
  return nodes;
};

const nodes = generateTreeJson();

fs.writeFileSync("data.json", JSON.stringify(nodes));
