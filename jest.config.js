module.exports = {
  collectCoverage: true,
  moduleNameMapper: {
    "^components/(.+)": "<rootDir>/components/$1",
    "^containers/(.+)": "<rootDir>/containers/$1",
    "^data/(.+)": "<rootDir>/data/$1",
    "^pages/(.+)": "<rootDir>/pages/$1",
  },
  verbose: true,
};
